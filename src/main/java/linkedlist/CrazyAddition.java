package linkedlist;

public class CrazyAddition {

    public static SinglyLinkedListNode<Integer> sum(SinglyLinkedListNode<Integer> n1, SinglyLinkedListNode<Integer> n2) {
        return buildListFromNumber(buildNumberFromList(n1) + buildNumberFromList(n2));
    }

    //This will not work with negative numbers
    public static SinglyLinkedListNode<Integer> sumWithoutConverting(SinglyLinkedListNode<Integer> n1, SinglyLinkedListNode<Integer> n2) {
        return sumWithoutConverting(n1, n2, 0);
    }

    private static SinglyLinkedListNode<Integer> sumWithoutConverting(SinglyLinkedListNode<Integer> n1, SinglyLinkedListNode<Integer> n2, int carryDigit) {
        if (n1 == null && n2 == null && carryDigit == 0) {
            return null;
        } else if (n1 == null && n2 == null) {
            int nextNodeValue = carryDigit % 10;
            int carryTheInt = carryDigit / 10;
            SinglyLinkedListNode<Integer> summedList = new SinglyLinkedListNode<>(nextNodeValue);
            summedList.add(sumWithoutConverting(null, null, carryTheInt));
            return summedList;
        } else if (n1 == null && carryDigit == 0) {
            return n2;
        } else if (n2 == null && carryDigit == 0) {
            return n1;
        } else if (n1 == null) {
            int sum = n2.getValue() + carryDigit;
            int nextNodeValue = sum % 10;
            int carryTheInt = sum / 10;
            SinglyLinkedListNode<Integer> summedList = new SinglyLinkedListNode<>(nextNodeValue);
            summedList.add(sumWithoutConverting(null, n2.getNext(), carryTheInt));
            return summedList;
        } else if (n2 == null) {
            int sum = n1.getValue() + carryDigit;
            int nextNodeValue = sum % 10;
            int carryTheInt = sum / 10;
            SinglyLinkedListNode<Integer> summedList = new SinglyLinkedListNode<>(nextNodeValue);
            summedList.add(sumWithoutConverting(n1.getNext(), null, carryTheInt));
            return summedList;
        } else {
            int sum = n1.getValue() + n2.getValue() + carryDigit;
            int nextNodeValue = sum % 10;
            int carryTheInt = sum / 10;
            SinglyLinkedListNode<Integer> summedList = new SinglyLinkedListNode<>(nextNodeValue);
            summedList.add(sumWithoutConverting(n1.getNext(), n2.getNext(), carryTheInt));
            return summedList;
        }
    }


    private static int buildNumberFromList(SinglyLinkedListNode<Integer> notANumberYet) {
        return buildNumberFromList(notANumberYet, 0);
    }

    private static int buildNumberFromList(SinglyLinkedListNode<Integer> notANumberYet, int tensPlace) {
        int multiplicand = (int) Math.pow(10, tensPlace);
        int accumulator = notANumberYet.getValue() * multiplicand;
        if (notANumberYet.hasNext())
            accumulator += buildNumberFromList(notANumberYet.getNext(), ++tensPlace);
        return accumulator;
    }


    private static SinglyLinkedListNode<Integer> buildListFromNumberEasy(int notAListYet) {
        char[] digitsAsChars = (notAListYet + "").toCharArray();
        int oneDigit = Integer.parseInt(Character.toString(digitsAsChars[digitsAsChars.length - 1]));
        SinglyLinkedListNode<Integer> list = new SinglyLinkedListNode<>(oneDigit);
        for (int i = digitsAsChars.length - 2; i > -1; i--) {
            oneDigit = Integer.parseInt(Character.toString(digitsAsChars[i]));
            list.add(oneDigit);
        }
        return list;
    }

    private static SinglyLinkedListNode<Integer> buildListFromNumber(int notAListYet) {
        if (notAListYet == 0)
            return null;

        int nextIntToAdd = notAListYet % 10;
        int nextIntToRecurOn = notAListYet / 10;
        SinglyLinkedListNode<Integer> list = new SinglyLinkedListNode<>(nextIntToAdd);
        list.add(buildListFromNumber(nextIntToRecurOn));
        return list;
    }

    private static void printFunctionResults(SinglyLinkedListNode<Integer> input1, SinglyLinkedListNode<Integer> input2) {
        try {
            SinglyLinkedListNode<Integer> result = sumWithoutConverting(input1, input2);
            System.out.println(String.format("Running sum() on inputs '%s' and '%s' returned : '%s'.", input1, input2, result));
        } catch (Exception e) {
            String unformatted = "Running sum() on inputs '%s' and '%s' threw '%s'.";
            System.out.println(String.format(unformatted, input1, input2, e));
        }
    }

    public static void main(String[] args) {
        SinglyLinkedListNode<Integer> n1 = new SinglyLinkedListNode<>(3);
        n1.add(7);
        n1.add(6);

        SinglyLinkedListNode<Integer> n2 = new SinglyLinkedListNode<>(7);
        n2.add(2);
        n2.add(3);

        printFunctionResults(n1, n2);


        n1 = new SinglyLinkedListNode<>(9);
        n1.add(9);
        n1.add(9);

        n2 = new SinglyLinkedListNode<>(1);

        printFunctionResults(n1, n2);


        n1 = new SinglyLinkedListNode<>(-3);
        n1.add(-7);
        n1.add(-6);

        n2 = new SinglyLinkedListNode<>(7);
        n2.add(2);
        n2.add(3);

        printFunctionResults(n1, n2);

        n1 = new SinglyLinkedListNode<>(5);
        n1.add(5);
        n1.add(5);
        n1.add(5);

        n2 = new SinglyLinkedListNode<>(7);
        n2.add(2);
        n2.add(3);

        printFunctionResults(n1, n2);



    }


}
