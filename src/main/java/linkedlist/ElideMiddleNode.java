package linkedlist;

public class ElideMiddleNode {

    //Method assumes the given node is not a first or a last node
    public static void elide(SinglyLinkedListNode node) {
        try {
            node.setValue(node.getNext().getValue());
            node.setNext(node.getNext().getNext());
        } catch(NullPointerException e) {
            throw new IndexOutOfBoundsException("Cannot elide the final node in a singly linked list!");
        }

    }

    private static void printFunctionResults(SinglyLinkedListNode input, SinglyLinkedListNode head) {
        try {
            System.out.println("Original state of containing list: " + head);
            System.out.println(String.format("Running elide() on input '%s'...", input));
            elide(input);
            System.out.println(String.format("It mutated head into '%s'.", head));
        } catch (Exception e) {
            System.out.println("Original state of containing list: "+ head);
            String unformatted = "Running elide() on input '%s' threw '%s'.";
            System.out.println(String.format(unformatted, input, e));
        }
    }

    public static void main(String[] args) {
        SinglyLinkedListNode<String> head = new SinglyLinkedListNode<String>("A");
        head.add("B");
        SinglyLinkedListNode<String> c = new SinglyLinkedListNode<>("C");
        head.add(c);
        head.add("D");

        printFunctionResults(c, head);


    }

}
