package linkedlist;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class RemoveDuplicates {

    public static <T> SinglyLinkedListNode<T> removeDuplicatesSet(SinglyLinkedListNode<T> list) {
        if (list == null)
            return null;
        List<T> buffer = new ArrayList<T>(list.size());
        buffer.add(list.getValue());
        SinglyLinkedListNode<T> movingListPointer = list;
        while (movingListPointer.getNext() != null) {
            movingListPointer = movingListPointer.getNext();
            if (!buffer.contains(movingListPointer.getValue()))
                buffer.add(movingListPointer.getValue());
        }
        movingListPointer = list;
        movingListPointer.setValue(buffer.get(0));
        for (int i = 1; i < buffer.size(); i++) {
            movingListPointer = movingListPointer.getNext();
            movingListPointer.setValue(buffer.get(i));
        }
        movingListPointer.setNext(null);
        return list;
    }

    public static <T> SinglyLinkedListNode<T> removeDuplicatesBuffer(SinglyLinkedListNode<T> list) {
        Object[] buffer = new Object[list.size()];
        buffer[0] = list.getValue();
        int currentBufferIndex = 1;
        SinglyLinkedListNode<T> movingListPointer = list;
        while (movingListPointer.getNext() != null) {
            movingListPointer = movingListPointer.getNext();
            if (!simpleContains(buffer, movingListPointer.getValue(), currentBufferIndex)) {
                buffer[currentBufferIndex] = movingListPointer.getValue();
                currentBufferIndex++;
            }
        }

        movingListPointer = list;
        movingListPointer.setValue((T) buffer[0]);
        for (int i = 1; i < currentBufferIndex; i++) {
            movingListPointer = movingListPointer.getNext();
            movingListPointer.setValue((T) buffer[i]);
        }
        movingListPointer.setNext(null);
        return list;
    }
    private static boolean simpleContains(Object[] array, Object value) {
        return simpleContains(array, value, array.length);
    }

    private static boolean simpleContains(Object[] array, Object value, int maxIndexToSearchExclusive) {
        for (int i = 0; i < maxIndexToSearchExclusive; i++) {
            Object o = array[i];
            if (o != null && o.equals(value))
                return true;
            else if (o == null && value == null)
                return true;
        }
        return false;
    }

    public static SinglyLinkedListNode removeDuplicatesNoBuffer(SinglyLinkedListNode list) {
        if (list != null)
            //By referencing list.next here, we continually advance the node we are using to kick off the helper method
            list.setNext(removeDuplicatesNoBuffer(removeAllByValue(list.getValue(), list.getNext())));
        return list;
    }

    //This helper method has the value of some previous node in the 'value' variable
    private static <T> SinglyLinkedListNode<T> removeAllByValue(T value, SinglyLinkedListNode<T> list) {
        if (list == null)
            return null;
        if (list.getValue() == value)
            return removeAllByValue(value, list.getNext());
        else {
            list.setNext(removeAllByValue(value, list.getNext()));
            return list;
        }
    }

    private static void printFunctionResults(Function<SinglyLinkedListNode, SinglyLinkedListNode> func, String funcName, SinglyLinkedListNode input) {
        System.out.println(String.format("Running "+funcName+"() on input '%s'...", input));
        SinglyLinkedListNode result = func.apply(input);
        System.out.println(String.format("It mutated the input into '%s'.", result));
    }


    public static void main(String[] args) {
        SinglyLinkedListNode<String> head = new SinglyLinkedListNode<String>("A");
        head.add("B");
        head.add("B");
        head.add("C");
        head.add("C");
        head.add("B");
        head.add("C");
        head.add("C");

        printFunctionResults(RemoveDuplicates::removeDuplicatesSet, "removeDuplicatesSet", head);

        SinglyLinkedListNode<String> newHead = new SinglyLinkedListNode<String>("A");

        printFunctionResults(RemoveDuplicates::removeDuplicatesSet, "removeDuplicatesSet", newHead);


        head = new SinglyLinkedListNode<String>("A");
        head.add("B");
        head.add("B");
        head.add("C");
        head.add("C");
        head.add("B");
        head.add("C");
        head.add("C");

        printFunctionResults(RemoveDuplicates::removeDuplicatesBuffer, "removeDuplicatesBuffer", head);

        newHead = new SinglyLinkedListNode<String>("A");

        printFunctionResults(RemoveDuplicates::removeDuplicatesBuffer, "removeDuplicatesBuffer", newHead);


        head = new SinglyLinkedListNode<String>("A");
        head.add("B");
        head.add("B");
        head.add("C");
        head.add("C");
        head.add("B");
        head.add("C");
        head.add("C");

        printFunctionResults(RemoveDuplicates::removeDuplicatesNoBuffer, "removeDuplicatesNoBuffer", head);

        newHead = new SinglyLinkedListNode<String>("A");

        printFunctionResults(RemoveDuplicates::removeDuplicatesNoBuffer, "removeDuplicatesNoBuffer", newHead);

    }

}
