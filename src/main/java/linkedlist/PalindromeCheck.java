package linkedlist;

public class PalindromeCheck {

    //This method uses buffers to store each half of the list. I could implement a solution that uses runners and
    // nested loops, but that'd have worse time complexity
    public static boolean isPalindrome(SinglyLinkedListNode list) {
        int totalSize = list.size();
        Object[] leftHalf = new Object[totalSize / 2];
        Object[] rightHalf = new Object[totalSize / 2];
        SinglyLinkedListNode movingPointer = list;
        for (int i = 0; i < totalSize; i++) {
            int insertionIndex;
            if (i < totalSize / 2) {
                insertionIndex = i;
                leftHalf[insertionIndex] = movingPointer.getValue();
            } else if (i >= totalSize - (totalSize / 2)) {
                insertionIndex = i - ((totalSize / 2) + (totalSize % 2));
                rightHalf[insertionIndex] = movingPointer.getValue();
            }
            movingPointer = movingPointer.getNext();
        }

        for (int j = 0; j < leftHalf.length; j++) {
            int rightHalfIndex = rightHalf.length - (j + 1);
            if (!leftHalf[j].equals(rightHalf[rightHalfIndex]))
                return false;
        }

        return true;
    }

    private static void printFunctionResults(SinglyLinkedListNode input) {
        try {
            boolean result = isPalindrome(input);
            System.out.println(String.format("Running isPalindrome() on input '%s' returned : '%s'.", input, result));
        } catch (Exception e) {
            String unformatted = "Running isPalindrome() on input '%s' threw '%s'.";
            System.out.println(String.format(unformatted, input, e));
        }
    }

    public static void main(String[] args) {

        SinglyLinkedListNode<String> n1 = new SinglyLinkedListNode<>("R");
        n1.add("A");
        n1.add("C");
        n1.add("E");
        n1.add("C");
        n1.add("A");
        n1.add("R");

        SinglyLinkedListNode<Integer> n2 = new SinglyLinkedListNode<>(1);
        n2.add(2);
        n2.add(3);
        n2.add(3);
        n2.add(2);
        n2.add(1);

        SinglyLinkedListNode<String> n3 = new SinglyLinkedListNode<>("R");
        n3.add("A");
        n3.add("C");
        n3.add("E");
        n3.add("C");
        n3.add("A");
        n3.add("R");
        n3.add("S");


        printFunctionResults(n1);
        printFunctionResults(n2);
        printFunctionResults(n3);

    }
}
