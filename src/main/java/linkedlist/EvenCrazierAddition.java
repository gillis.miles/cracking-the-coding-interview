package linkedlist;

public class EvenCrazierAddition {

    public static SinglyLinkedListNode<Integer> sum(SinglyLinkedListNode<Integer> n1, SinglyLinkedListNode<Integer> n2) {
        int lengthDifference = n1.size() - n2.size();
        if (lengthDifference < 0) {
            for (int i = 0; i > lengthDifference; i--)
                n1 = new SinglyLinkedListNode<>(0, n1);
        } else if (lengthDifference > 0) {
            for (int i = 0; i < lengthDifference; i++)
                n2 = new SinglyLinkedListNode<>(0, n2);
        }
        MyTuple allNodesExamined = sumSameLengthNumbers(n1, n2);
        int carryAmount = allNodesExamined.carryDigit;
        SinglyLinkedListNode<Integer> mostOfTheList = allNodesExamined.theRest;
        while (carryAmount > 0) {
            mostOfTheList = new SinglyLinkedListNode<>(carryAmount % 10, mostOfTheList);
            carryAmount = carryAmount / 10;
        }
        return mostOfTheList;
    }

    //n1 and n2 MUST be the same length
    private static MyTuple sumSameLengthNumbers(SinglyLinkedListNode<Integer> n1, SinglyLinkedListNode<Integer> n2) {
        if (n1 == null)
            return null;
        if (!n1.hasNext()) {
            int sum = n1.getValue() + n2.getValue();
            SinglyLinkedListNode<Integer> rightMostNode = new SinglyLinkedListNode<>(sum % 10);
            int carryDigit = sum / 10;
            return new MyTuple(rightMostNode, carryDigit);
        } else {
            MyTuple allRightNodesTuple = sumSameLengthNumbers(n1.getNext(), n2.getNext());
            SinglyLinkedListNode<Integer> allRightNodes = allRightNodesTuple.theRest;
            int sum = n1.getValue() + n2.getValue() + allRightNodesTuple.carryDigit;
            SinglyLinkedListNode<Integer> nextNode = new SinglyLinkedListNode<>(sum % 10, allRightNodes);
            int carryDigit = sum / 10;
            return new MyTuple(nextNode, carryDigit);
        }
    }

    private static void printFunctionResults(SinglyLinkedListNode<Integer> input1, SinglyLinkedListNode<Integer> input2) {
        try {
            SinglyLinkedListNode<Integer> result = sum(input1, input2);
            System.out.println(String.format("Running sum() on inputs '%s' and '%s' returned : '%s'.", input1, input2, result));
        } catch (Exception e) {
            String unformatted = "Running sum() on inputs '%s' and '%s' threw '%s'.";
            System.out.println(String.format(unformatted, input1, input2, e));
        }
    }

    public static void main(String[] args) {
        SinglyLinkedListNode<Integer> n1 = new SinglyLinkedListNode<>(3);
        n1.add(7);
        n1.add(6);

        SinglyLinkedListNode<Integer> n2 = new SinglyLinkedListNode<>(7);
        n2.add(2);
        n2.add(3);

        printFunctionResults(n1, n2);


        n1 = new SinglyLinkedListNode<>(9);
        n1.add(9);
        n1.add(9);

        n2 = new SinglyLinkedListNode<>(1);

        printFunctionResults(n1, n2);


        n1 = new SinglyLinkedListNode<>(-3);
        n1.add(-7);
        n1.add(-6);

        n2 = new SinglyLinkedListNode<>(7);
        n2.add(2);
        n2.add(3);

        printFunctionResults(n1, n2);

        n1 = new SinglyLinkedListNode<>(5);
        n1.add(5);
        n1.add(5);
        n1.add(5);

        n2 = new SinglyLinkedListNode<>(7);
        n2.add(2);
        n2.add(3);

        printFunctionResults(n1, n2);
    }

    private static class MyTuple {
        public SinglyLinkedListNode<Integer> theRest;
        public int carryDigit;

        public MyTuple(SinglyLinkedListNode<Integer> theRest, int carryDigit) {
            this.theRest = theRest;
            this.carryDigit = carryDigit;
        }
    }
}
