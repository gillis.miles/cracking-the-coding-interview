package linkedlist;

import com.sun.istack.internal.NotNull;

//Remove ability to add as a node, only add by value
//Break node and list into two classes?

public class SinglyLinkedListNode<T> {
    @NotNull
    private T value;
    private SinglyLinkedListNode<T> next = null;

    public SinglyLinkedListNode(@NotNull T value) {
        this.value = value;
    }

    public SinglyLinkedListNode(@NotNull T value, SinglyLinkedListNode<T> next) {
        this.value = value;
        this.next = next;
    }

    public void add(@NotNull T toAppend) {
        add(new SinglyLinkedListNode<T>(toAppend));
    }

    public void add(@NotNull SinglyLinkedListNode toAppend) {
        if (this.next == null)
            this.next = toAppend;
        else
            this.next.add(toAppend);
    }

    public int size() {
        if (next != null)
            return 1 + next.size();
        else
            return 1;
    }


    public boolean isTail() {
        return next == null;
    }

    public SinglyLinkedListNode<T> getTail() {
        if(this.isTail())
            return this;
        else return this.next.getTail();
    }


    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        if (value == null)
            throw new RuntimeException("The value of a SinglyLinkedListNode cannot be null!");
        this.value = value;
    }

    public boolean hasNext() {
        return next != null;
    }

    public SinglyLinkedListNode<T> getNext() {
        return next;
    }

    public void setNext(SinglyLinkedListNode<T> next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return next == null ? value.toString() : value.toString() + ", " + next.toString();
    }
}


class LinkedListTest {


    public static void main(String[] args) {
        SinglyLinkedListNode<String> head = new SinglyLinkedListNode<String>("A");
        head.add("B");
        head.add("C");

        System.out.println(head);

        //Stack overflow yay! Caused by a cyclical list
        SinglyLinkedListNode<String> cycle1 = new SinglyLinkedListNode<String>("A");
        SinglyLinkedListNode<String> cycle2 = new SinglyLinkedListNode<String>("B");
        SinglyLinkedListNode<String> cycle3 = new SinglyLinkedListNode<String>("C");
        SinglyLinkedListNode<String> cycle4 = new SinglyLinkedListNode<String>("D");
        cycle1.add(cycle2);
        cycle1.add(cycle3);
        cycle1.add(cycle4);
        cycle1.add(cycle1);

        System.out.println(cycle1);

    }
}
