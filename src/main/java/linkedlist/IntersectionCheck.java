package linkedlist;

public class IntersectionCheck {

    //We can just compare the tails, because once an intersection occurs, it can't be untangled
    public static boolean haveIntersection(SinglyLinkedListNode l1, SinglyLinkedListNode l2) {
        return l1.getTail() == l2.getTail();
    }

    private static void printFunctionResults(SinglyLinkedListNode input1, SinglyLinkedListNode input2) {
        try {
            boolean result = haveIntersection(input1, input2);
            System.out.println(String.format("Running haveIntersection() on inputs '%s' and '%s' returned : '%s'.", input1, input2, result));
        } catch (Exception e) {
            String unformatted = "Running haveIntersection() on inputs '%s' and '%s' threw '%s'.";
            System.out.println(String.format(unformatted, input1, input2, e));
        }
    }

    public static void main(String[] args) {

        SinglyLinkedListNode<String> sameTail = new SinglyLinkedListNode<>("same");
        sameTail.add("same");
        sameTail.add("same");


        SinglyLinkedListNode<String> n1 = new SinglyLinkedListNode<>("different");
        n1.add("different");
        n1.add("different");
        n1.add("different");
        n1.add("different");
        n1.add("different");

        SinglyLinkedListNode<String> n2 = new SinglyLinkedListNode<>("differenter");
        n2.add("differenter");

        printFunctionResults(n1, n2);

        n1.add(sameTail);
        n2.add(sameTail);
        printFunctionResults(n1, n2);
    }

}
