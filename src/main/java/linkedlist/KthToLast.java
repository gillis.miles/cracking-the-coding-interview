package linkedlist;

public class KthToLast {

    public static <T> T kthToLast(SinglyLinkedListNode<T> list, int k) {
        int size = list.size();
        int desiredIndex = size - k;
        if (desiredIndex < 0 || desiredIndex >= size)
            throw new IndexOutOfBoundsException();

        SinglyLinkedListNode<T> movingListPointer = list;
        T movingValue = null;
        for (int i = 0; i <= desiredIndex; i++) {
            movingValue = movingListPointer.getValue();
            movingListPointer = movingListPointer.getNext();
        }
        return movingValue;
    }

    private static void printFunctionResults(SinglyLinkedListNode input1, int input2) {
        try {
            Object result = kthToLast(input1, input2);
            String unformatted = "Running kthToLast() on inputs '%s' and '%s' returned '%s'.";
            System.out.println(String.format(unformatted, input1, input2, result));
        } catch (Exception e) {
            String unformatted = "Running kthToLast() on inputs '%s' and '%s' threw '%s'.";
            System.out.println(String.format(unformatted, input1, input2, e));
        }
    }

    public static void main(String[] args) {
        SinglyLinkedListNode<String> head = new SinglyLinkedListNode<String>("A");
        head.add("B");
        head.add("C");
        head.add("D");
        head.add("E");
        head.add("F");
        head.add("G");
        head.add("H");

        printFunctionResults(head, 1);
        printFunctionResults(head, 6);
        printFunctionResults(head, 8);
        printFunctionResults(head, 0);
        printFunctionResults(head, 9);

    }

}
