package linkedlist;

public class Partition {

    //I'm going to build a new list because it doesn't say that I have to do this one in place
    //Could also get fancy with this and use a comparator, decoupling this from the Integer data type
    public static SinglyLinkedListNode<Integer> partition(SinglyLinkedListNode<Integer> list, int partitionValue) {
        if(list == null)
            return null;
        SinglyLinkedListNode<Integer> partitionedList = new SinglyLinkedListNode<>(list.getValue());
        SinglyLinkedListNode<Integer> movingListPointer = list;
        while(movingListPointer.getNext() != null) {
            movingListPointer = movingListPointer.getNext();
            if(movingListPointer.getValue() < partitionValue) {
                SinglyLinkedListNode<Integer> theRest = partitionedList;
                partitionedList = new SinglyLinkedListNode<>(movingListPointer.getValue());
                partitionedList.add(theRest);
            } else {
                partitionedList.add(movingListPointer.getValue());
            }
        }
        return partitionedList;
    }

    private static void printFunctionResults(SinglyLinkedListNode<Integer> input, int partitionValue) {
        try {
            SinglyLinkedListNode<Integer> partitioned = partition(input, partitionValue);
            System.out.println(String.format("Running partition() on inputs '%s' and '%s' returned : '%s'.", input, partitionValue, partitioned));
        } catch (Exception e) {
            String unformatted = "Running partition() on inputs '%s' and '%s' threw '%s'.";
            System.out.println(String.format(unformatted, input, partitionValue, e));
        }
    }

    public static void main(String[] args) {
        SinglyLinkedListNode<Integer> original = new SinglyLinkedListNode<>(3);
        original.add(8);
        original.add(7);
        original.add(5);
        original.add(2);
        original.add(11);
        original.add(-1);
        original.add(6);

        printFunctionResults(original, 5);
        printFunctionResults(original, -2);
        printFunctionResults(original, 12);

    }

}
