package stacks_and_queues;

public interface Queue<T> {

    void enqueue(T toAdd);

    T dequeue();

    T peek();

    boolean isEmpty();

}
