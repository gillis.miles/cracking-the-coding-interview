package stacks_and_queues;

public class StackSort {

    public static <T extends Comparable<T>> Stack<T> sort(Stack<T> aStack) {
        Stack<T> buffer = new MyStack<>();
        int size = 0;
        T largest = null;

        //First pass
        while (!aStack.isEmpty()) {
            T next = aStack.pop();
            if (largest == null) {
                largest = next;
            } else if (next.compareTo(largest) > 0) {
                buffer.push(largest);
                largest = next;
            } else {
                buffer.push(next);
            }
            size++;
        }
        aStack.push(largest);

        while (!buffer.isEmpty())
            aStack.push(buffer.pop());

        for (int i = size - 1; i > 1; i--)
            sortHelper(aStack, buffer, i);

        return aStack;
    }

    private static <T extends Comparable<T>> void sortHelper(Stack<T> aStack, Stack<T> bStack, int maxSteps) {
        //aStack should be our partial solution
        //bStack should be a buffer, an empty stack
        T maxSoFar = aStack.pop();
        for (int i = 1; i < maxSteps; i++) {
            T next = aStack.pop();
            if (next.compareTo(maxSoFar) > 0) {
                bStack.push(maxSoFar);
                maxSoFar = next;
            } else {
                bStack.push(next);
            }
        }
        aStack.push(maxSoFar);
        while (!bStack.isEmpty())
            aStack.push(bStack.pop());
    }

    public static void main(String[] args) {
        Stack<String> aStack = new MyStack<>();
        aStack.push("camel");
        aStack.push("donkey");
        aStack.push("albatross");
        aStack.push("bear");
        aStack.push("emu");

        System.out.println("Stack before: " + aStack);
        sort(aStack);
        System.out.println("Stack after: " + aStack);
    }

}
