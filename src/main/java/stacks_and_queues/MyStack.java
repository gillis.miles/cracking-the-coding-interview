package stacks_and_queues;

public class MyStack<T> implements Stack<T> {

    protected static class StackNode<T> {
        protected T data;
        protected StackNode<T> next;


        protected StackNode(T data) {
            this.data = data;
        }

        protected StackNode(T data, StackNode<T> next) {
            this.data = data;
            this.next = next;
        }

        public String toString() {
            return data.toString() + " " + (next == null ? "" : next.toString());
        }

    }

    protected StackNode<T> top;

    public MyStack() {
        this.top = null;
    }

    public MyStack(T first) {
        this.top = new StackNode<>(first);
    }

    public T pop() {
        if (top == null)
            throw new RuntimeException("Cannot pop a value from an empty stack");
        T toPop = top.data;
        top = top.next;
        return toPop;
    }

    public void push(T toAdd) {
        this.top = new StackNode<>(toAdd, top);
    }

    public T peek() {
        if (top == null)
            throw new RuntimeException("Cannot peek a value from an empty stack");
        return top.data;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public String toString() {
        return "[" + (top == null ? "" : top.toString().trim()) + "]";
    }
}
