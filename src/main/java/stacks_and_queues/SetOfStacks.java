package stacks_and_queues;

import java.util.ArrayList;
import java.util.List;

public class SetOfStacks<T> implements Stack<T> {

    private final int maxStackHeight;
    private List<Stack<T>> stacks;
    private List<Integer> stackHeights;

    public SetOfStacks(int maxStackHeight) {
        if (maxStackHeight < 1)
            throw new RuntimeException("Maximum stack height must be greater than 0 you idiot.");
        this.maxStackHeight = maxStackHeight;
        this.stacks = new ArrayList<>();
        this.stacks.add(new MyStack<>());
        this.stackHeights = new ArrayList<>();
        this.stackHeights.add(0);
    }

    @Override
    public void push(T toAdd) {
        int currentStackHeight = stackHeights.get(stackHeights.size() - 1);
        if (currentStackHeight == maxStackHeight) {
            Stack<T> newStack = new MyStack<>(toAdd);
            stacks.add(newStack);
            stackHeights.add(1);
        } else {
            Stack<T> currentStack = stacks.get(stacks.size() - 1);
            currentStack.push(toAdd);
            stackHeights.set(stackHeights.size() - 1, currentStackHeight + 1);
        }
    }

    @Override
    public T pop() {
        return popAt(stacks.size() - 1);
    }

    public T popAt(int index) {
        if (index >= stacks.size())
            throw new IndexOutOfBoundsException("There are only " + stacks.size()
                    + " stacks in this SetOfStacks but the sub stack at index " + index + " was requested");
        Stack<T> currentStack = stacks.get(index);
        T popped = currentStack.pop();
        if (currentStack.isEmpty()) {
            stacks.remove(index);
            stackHeights.remove(index);
        } else {
            int currentStackHeight = stackHeights.get(index);
            stackHeights.set(index, currentStackHeight - 1);
        }
        return popped;

    }

    @Override
    public T peek() {
        return stacks.get(stacks.size() - 1).peek();
    }

    @Override
    public boolean isEmpty() {
        return stacks.isEmpty() || stacks.get(0).isEmpty();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for(Stack s : stacks)
            sb.append(s.toString());
        return sb.append("]").toString();
    }

    public static void main(String[] args) {
        SetOfStacks<String> somePlates = new SetOfStacks<>(3);
        somePlates.push("A");
        somePlates.push("B");
        somePlates.push("C");
        somePlates.push("D");
        somePlates.push("E");
        somePlates.push("F");
        somePlates.push("G");

        System.out.println("somePlates looks like this: " + somePlates);

        somePlates.pop();
        somePlates.pop();

        System.out.println("somePlates looks like this: " + somePlates);


        SetOfStacks<String> otherPlates = new SetOfStacks<>(3);
        otherPlates.push("A");
        otherPlates.push("B");
        otherPlates.push("C");
        otherPlates.push("D");
        otherPlates.push("E");
        otherPlates.push("F");
        otherPlates.push("G");

        System.out.println("otherPlates looks like this: " + otherPlates);

        otherPlates.popAt(1);
        otherPlates.popAt(1);
        otherPlates.popAt(0);
        otherPlates.popAt(0);


        System.out.println("otherPlates looks like this: " + otherPlates);

        otherPlates.pop();
        otherPlates.pop();

        System.out.println("otherPlates looks like this: " + otherPlates);
    }
}
