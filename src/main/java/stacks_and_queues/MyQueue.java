package stacks_and_queues;

public class MyQueue<T> implements Queue<T> {

    private static class QueueNode<T> {
        private T data;
        private QueueNode<T> next;


        public QueueNode(T data) {
            this.data = data;
        }

    }

    private QueueNode<T> first;
    private QueueNode<T> last;

    public MyQueue() {
        this.first = null;
        this.last = null;
    }

    public MyQueue(T first) {
        QueueNode<T> onlyNode = new QueueNode<>(first);
        this.first = onlyNode;
        this.last = onlyNode;
    }

    public T dequeue() {
        if (first == null)
            throw new RuntimeException("Cannot pop a value from an empty stack");
        T dequeued = first.data;
        first = first.next;
        if (first == null)
            last = null;
        return dequeued;
    }

    public void enqueue(T toAdd) {
        QueueNode<T> newLast = new QueueNode<>(toAdd);
        if (last != null)
            this.last.next = newLast;
        this.last = newLast;
        if (first == null)
            this.first = newLast;
    }

    public T peek() {
        if (first == null)
            throw new RuntimeException("Cannot peek a value from an empty stack");
        return first.data;
    }

    public boolean isEmpty() {
        return first == null;
    }
}
