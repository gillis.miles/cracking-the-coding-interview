package stacks_and_queues;

import java.util.HashMap;
import java.util.Map;

public class FrequencyStack {


    private Map<Integer, MyStack<Integer>> frequencyToValues = new HashMap<>();
    private Map<Integer, Integer> valueToFrequency = new HashMap<>();
    private int maxFrequency = 0;

    public void push(Integer toAdd) {
        int newFrequency = valueToFrequency.getOrDefault(toAdd, 0) + 1;
        valueToFrequency.put(toAdd, newFrequency);
        MyStack<Integer> existingStack = frequencyToValues.get(newFrequency);
        if(existingStack != null)
            existingStack.push(toAdd);
        else
            frequencyToValues.put(newFrequency, new MyStack<>(toAdd));
        if (newFrequency > maxFrequency)
            maxFrequency = newFrequency;
    }

    public Integer pop() {
        if(maxFrequency == 0)
            throw new RuntimeException("Cannot pop a value from an empty stack");
        MyStack<Integer> stackToPopFrom = frequencyToValues.get(maxFrequency);
        int toPop = stackToPopFrom.pop();
        int newFrequency = valueToFrequency.get(toPop) - 1;
        valueToFrequency.put(toPop, newFrequency);
        if(stackToPopFrom.isEmpty())
            maxFrequency--;
        return toPop;
    }

    public static void main(String[] args) {

        // 1.
        // push(1), push(2), push(1), push(3), pop() -> return 1, pop() -> return 3

        // 2.
        // push(1), push(2), push(2), push(1), push(3), pop() -> return 1, pop() -> return 2

        FrequencyStack stack = new FrequencyStack();
        stack.push(1);
        stack.push(2);
        stack.push(1);
        stack.push(3);

        int popped = stack.pop();

        System.out.println("Popped value: " + popped + ". Expected 1.");

        popped = stack.pop();
        System.out.println("Popped value: " + popped + ". Expected 3.");

        stack = new FrequencyStack();
        stack.push(1);
        stack.push(2);
        stack.push(2);
        stack.push(1);
        stack.push(3);

        popped = stack.pop();

        System.out.println("Popped value: " + popped + ". Expected 1.");

        popped = stack.pop();
        System.out.println("Popped value: " + popped + ". Expected 2.");


    }

}