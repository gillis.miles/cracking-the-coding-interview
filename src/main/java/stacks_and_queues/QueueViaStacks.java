package stacks_and_queues;

//I needed to read the hints and then the answer for this one before I could see how to implement it.
public class QueueViaStacks<T> implements Queue<T> {

    private Stack<T> newToOld;
    private Stack<T> oldToNew;

    public QueueViaStacks() {
        newToOld = new MyStack<>();
        oldToNew = new MyStack<>();
    }

    @Override
    public void enqueue(T toAdd) {
        newToOld.push(toAdd);
    }

    @Override
    public T dequeue() {
        if (oldToNew.isEmpty())
            shift();
        return oldToNew.pop();
    }

    @Override
    public T peek() {
        if (oldToNew.isEmpty())
            shift();
        return oldToNew.peek();
    }

    private void shift() {
        while (!newToOld.isEmpty())
            oldToNew.push(newToOld.pop());
    }

    @Override
    public boolean isEmpty() {
        return newToOld.isEmpty() && oldToNew.isEmpty();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        String oldToNewString = oldToNew.toString();
        String newToOldString = newToOld.toString();
        sb.append(oldToNewString, 1, oldToNewString.length() - 1).append(" ");
        for (int i = newToOldString.length() - 2; i > 0; i--)
            sb.append(newToOldString.charAt(i)).append(" ");
        return sb.toString().trim() + "]";
    }


    public static void main(String[] args) {

        QueueViaStacks<String> q = new QueueViaStacks<>();
        q.enqueue("A");
        q.enqueue("B");
        q.enqueue("C");
        q.enqueue("C");
        q.enqueue("C");
        q.dequeue();
        q.dequeue();
        q.enqueue("D");

        System.out.println("QueueViaStacks looks like this: "  + q);

        QueueViaStacks<String> q2 = new QueueViaStacks<>();
        q2.enqueue("C");
        q2.enqueue("C");
        q2.enqueue("A");
        q2.enqueue("B");
        q2.enqueue("C");
        q2.dequeue();
        q2.dequeue();
        q2.enqueue("D");

        System.out.println("QueueViaStacks looks like this: "  + q2);



    }
}
