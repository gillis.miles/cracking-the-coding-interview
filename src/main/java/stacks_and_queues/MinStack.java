package stacks_and_queues;

import com.sun.istack.internal.NotNull;

public class MinStack<T extends Comparable<T>> implements Stack<T> {

    protected static class StackNode<T extends Comparable<T>> {
        protected T data;
        protected StackNode<T> next;
        protected StackNode<T> nextMin;

        public StackNode(@NotNull T data) {
            this.data = data;
            this.next = null;
            this.nextMin = this;
        }

        public StackNode(@NotNull T data, StackNode<T> next) {
            this.data = data;
            this.next = next;
            this.nextMin = this;
        }

        public StackNode(@NotNull T data, StackNode<T> next, @NotNull StackNode<T> nextMin) {
            this.data = data;
            this.next = next;
            this.nextMin = nextMin;
        }

        public String toString() {
            return data.toString() + " " + (next == null ? "" : next.toString());
        }

    }

    protected StackNode<T> top;

    public MinStack() {
        this.top = null;
    }

    public MinStack(T first) {
        this.top = new StackNode<>(first);
    }

    public T pop() {
        if (top == null)
            throw new RuntimeException("Cannot pop a value from an empty stack");
        T toPop = top.data;
        top = top.next;
        return toPop;
    }

    public void push(T toAdd) {
        if(toAdd.compareTo(top.nextMin.data) < 0) {
            top = new StackNode<>(toAdd, top);
        } else {
            top = new StackNode<>(toAdd, top, top.nextMin);
        }
    }

    public T min() {
        if (top == null)
            throw new RuntimeException("There is no minimum value of an empty stack");
        return top.nextMin.data;
    }

    public T peek() {
        if (top == null)
            throw new RuntimeException("Cannot peek a value from an empty stack");
        return top.data;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public String toString() {
        return "[" + (top == null ? "" : top.toString().trim()) + "]";
    }


    public static void main(String[] args) {

        MinStack<Integer> decreasing = new MinStack<>(1);
        decreasing.push(2);
        decreasing.push(3);
        decreasing.push(4);
        decreasing.push(5);

        System.out.println("For stack " + decreasing + "...");
        System.out.println("The minimum value is " + decreasing.min());

        MinStack<Integer> increasing = new MinStack<>(5);
        increasing.push(4);
        increasing.push(3);
        increasing.push(2);
        increasing.push(1);

        System.out.println("For stack " + increasing + "...");
        System.out.println("The minimum value is " + increasing.min());

        increasing.push(5);
        increasing.push(4);

        System.out.println("For stack " + increasing + "...");
        System.out.println("The minimum value is " + increasing.min());

        increasing.pop();
        increasing.pop();
        increasing.pop();

        System.out.println("For stack " + increasing + "...");
        System.out.println("The minimum value is " + increasing.min());

    }
}
