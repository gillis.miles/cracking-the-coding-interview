package stacks_and_queues;

public interface Stack<T> {

    void push(T toAdd);

    T pop();

    T peek();

    boolean isEmpty();

    String toString();

}
