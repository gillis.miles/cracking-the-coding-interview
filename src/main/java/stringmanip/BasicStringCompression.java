package stringmanip;

public class BasicStringCompression {

    public static String compress(String original) {
        StringBuilder sb = new StringBuilder();
        char currentChar = original.charAt(0);
        int charCount = 1;
        for(int i = 1; i < original.length(); i++) {
            char nextChar = original.charAt(i);
            if(nextChar == currentChar) {
                charCount++;
            } else {
                sb.append(currentChar);
                sb.append(charCount);
                currentChar = nextChar;
                charCount = 1;
            }
        }
        sb.append(currentChar);
        sb.append(charCount);

        if (sb.length() >= original.length())
            return original;
        return sb.toString();
    }


    private static void printFunctionResults(String input) {
        String result = compress(input);
        String unformatted = "Running compress() on input '%s' returned '%s'.";
        System.out.println(String.format(unformatted, input, result));
    }

    public static void main(String[] args) {

        String test1 = "abcdef";
        String test2 = "abcdeeeeeeefghhhhhiiiiiii";
        String test3 = "aabbccddeeff";
        String test4 = "aabbccddeefff";
        String test5 = "abbccddeefff";
        String test6 = "a";

        printFunctionResults(test1);
        printFunctionResults(test2);
        printFunctionResults(test3);
        printFunctionResults(test4);
        printFunctionResults(test5);
        printFunctionResults(test6);

    }
}
