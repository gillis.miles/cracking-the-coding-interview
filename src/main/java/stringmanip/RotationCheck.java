package stringmanip;

public class RotationCheck {

    public static boolean isRotation(String s1, String s2) {
        if(s1 == null || s2 == null || s1.length() != s2.length())
            return false;
        if(s1.equals(s2))
            return true;

        for(int i = 1; i < s2.length(); i++) {
            String firstPart = s2.substring(0, i);
            String secondPart = s2.substring(i);
            if(s1.equals(secondPart + firstPart))
                return true;
        }
        return false;
    }

    private static void printFunctionResults(String input1, String input2) {
         boolean result = isRotation(input1, input2);
        String unformatted = "Running isRotation() on inputs '%s' and '%s' returned '%s'.";
        System.out.println(String.format(unformatted, input1, input2, result));
    }


    public static void main(String[] args) {
        String testSuccess1 = "waterbottle";
        String testSuccess2 = "erbottlewat";
        String testFailure1 = "abcdefg";
        String testFailure2 = "gabcdefg";

        printFunctionResults(testSuccess1, testSuccess2);
        printFunctionResults(testFailure1, testFailure2);

    }
}
