package stringmanip;

public class OneEditAway {

    public static boolean areStringsOneEditAway(String s1, String s2) {
        //if s2 is one character shorter, check that all characters in s2 are in s1, going in order, and only skipping one index of s1
        //if s2 is one character longer, check that all character in s1 are in s2, going in order, and only skipping one index of s1
        //if they're the same length, traverse them simultaneously. Return false if more than 1 difference is found
        // otherwise, false

        if(s1 == null || s2 == null)
            return false;

        int lengthDiff = s1.length() - s2.length();
        int s1Index = 0;
        int s2Index = 0;
        int differencesFound = 0;
        if(lengthDiff == 1) {
            //s1 is longer by 1
            //Advance s1's index by 1 on first difference
            while(differencesFound < 2 && s1Index < s1.length() && s2Index < s2.length()){
                char s1Char = s1.charAt(s1Index);
                char s2Char = s2.charAt(s2Index);
                if(s1Char == s2Char) {
                    s1Index++;
                    s2Index++;
                } else {
                    differencesFound++;
                    s1Index++;
                }
            }
            return differencesFound < 2;

        } else if(lengthDiff == -1) {
            //s2 is longer by 1
            while(differencesFound < 2 && s1Index < s1.length() && s2Index < s2.length()){
                char s1Char = s1.charAt(s1Index);
                char s2Char = s2.charAt(s2Index);
                if(s1Char == s2Char) {
                    s1Index++;
                    s2Index++;
                } else {
                    differencesFound++;
                    s2Index++;
                }
            }
            return differencesFound < 2;

        } else if(lengthDiff == 0) {
            while(differencesFound < 2 && s1Index < s1.length() && s2Index < s2.length()) {
                char s1Char = s1.charAt(s1Index);
                char s2Char = s2.charAt(s2Index);
                if (s1Char == s2Char) {
                    s1Index++;
                    s2Index++;
                } else {
                    differencesFound++;
                    s1Index++;
                    s2Index++;
                }
            }
            return differencesFound < 2;

        } else return false;
    }

    public static boolean areStringsOneEditAwayMoBetta(String s1, String s2) {
        if (s1 == null || s2 == null)
            return false;

        int lengthDifference = s1.length() - s2.length();
        if (Math.abs(lengthDifference) > 1) {
            return false;
        }

        int s1Index = 0;
        int s2Index = 0;
        int differencesFound = 0;

        while(differencesFound < 2 && s1Index < s1.length() && s2Index < s2.length()) {
            char s1Char = s1.charAt(s1Index);
            char s2Char = s2.charAt(s2Index);
            if (s1Char == s2Char) {
                s1Index++;
                s2Index++;
            } else {
                differencesFound++;
                s1Index = lengthDifference == -1 ? s1Index : s1Index + 1;
                s2Index = lengthDifference == 1 ? s2Index : s2Index + 1;
            }
        }
        return differencesFound < 2;

    }
    private static void printFunctionResults(String s1, String s2) {
        Boolean result = areStringsOneEditAway(s1,  s2);
        Boolean result2 = areStringsOneEditAwayMoBetta(s1,  s2);
        String unformatted = "Running areStringsOneEditAway() on inputs '%s' and '%s' returned '%s'.";
        String unformatted2 = "Running areStringsOneEditAwayMoBetta() on inputs '%s' and '%s' returned '%s'.";
        System.out.println(String.format(unformatted, s1, s2, result));
        System.out.println(String.format(unformatted2, s1, s2, result2));
    }

    public static void main(String[] args) {

        String pale = "pale";
        String ple = "ple";
        String pales = "pales";
        String bale = "ple";
        String bake = "bake";

        printFunctionResults(pale, ple);
        printFunctionResults(pales, pale);
        printFunctionResults(pale, bale);
        printFunctionResults(pale, bake);
        printFunctionResults("", "a");
        printFunctionResults("", "");
        printFunctionResults("ab", "");

    }


}
