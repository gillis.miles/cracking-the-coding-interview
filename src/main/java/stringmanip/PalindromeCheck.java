package stringmanip;

import com.sun.istack.internal.NotNull;

public class PalindromeCheck {

    private static final String alphaNumeric = "abcdefghijklmnopqrstuvwxyz1234567890";

    public static boolean weakPalindromCheck(@NotNull String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            if (s.charAt(i) != s.charAt(s.length() - i - 1))
                return false;
        }
        return true;
    }

    public static boolean palindromeCheck(@NotNull String s) {
        String string = s.toLowerCase();
        int leftIndex = 0;
        int rightIndex = s.length() - 1;
        char leftChar, rightChar;
        while(leftIndex <= rightIndex){
            leftChar = string.charAt(leftIndex);
            rightChar = string.charAt(rightIndex);
            if(alphaNumeric.indexOf(leftChar) < 0) {
                leftIndex++;
            } else if(alphaNumeric.indexOf(rightChar) < 0) {
                rightIndex--;
            } else if(leftChar != rightChar) {
                return false;
            } else {
                leftIndex++;
                rightIndex--;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String racecar = "racecar";
        String symmetry = "abcddcba";
        String notAPallindrome = "not a palindrome";
        String longPallindrom = "go hang a salami I'm a lasagna hog!";

        System.out.println("Is '" + racecar + "' a palindrome? " + String.valueOf(weakPalindromCheck(racecar)).toUpperCase());
        System.out.println("Is '" + symmetry + "' a palindrome? " + String.valueOf(weakPalindromCheck(symmetry)).toUpperCase());
        System.out.println("Is '" + notAPallindrome + "' a palindrome? " + String.valueOf(weakPalindromCheck(notAPallindrome)).toUpperCase());
        System.out.println("Is '" + longPallindrom + "' a palindrome? " + String.valueOf(palindromeCheck(longPallindrom)).toUpperCase());
    }

}
