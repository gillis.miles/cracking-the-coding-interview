package stringmanip;

import java.util.Arrays;

public class PermutationCheck {

    //We could probably use an additive HashMap to do this in O(3n) instead of O(2nlogn + whatever array equality check is)
    public static boolean isPermutation(String a, String b) {
        if(a.length() != b.length())
            return false;

        char[] aCharArray = a.toCharArray();
        char[] bCharArray = b.toCharArray();
        Arrays.sort(aCharArray);
        Arrays.sort(bCharArray);

        return Arrays.equals(aCharArray, bCharArray);
    }

    public static void main(String[] args){
        String test1a = "abcdefg";
        String test1b = "bgfedac";

        String test2a = "abcdefg";
        String test2b = "abcdefgg";

        System.out.println("String '" + test1a + "' and '" +test1b+ "' are permutations: " + isPermutation(test1a, test1b));
        System.out.println("String '" + test2a + "' and '" +test2b+ "' are permutations: " + isPermutation(test2a, test2b));
    }
}
