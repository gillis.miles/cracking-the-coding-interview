package stringmanip;

public class RotateMatrix {

    public static void rotateClockwise(String[][] matrix) {
        verifyMatrixIsSquare(matrix);

        String popFirst;
        int maxIndex = matrix.length - 1;
        for (int x = 0; x < matrix.length / 2; x++) {
            for (int y = x; y < maxIndex - x; y++) {
                popFirst = matrix[y][x];
                matrix[y][x] = matrix[maxIndex - x][y];
                matrix[maxIndex - x][y] = matrix[maxIndex - y][maxIndex - x];
                matrix[maxIndex - y][maxIndex - x] = matrix[x][maxIndex - y];
                matrix[x][maxIndex - y] = popFirst;
            }
        }
    }

    public static void rotateCounterClockwise(String[][] matrix) {
        verifyMatrixIsSquare(matrix);

        String popFirst;
        int maxIndex = matrix.length - 1;
        for (int x = 0; x < matrix.length / 2; x++) {
            for (int y = x; y < maxIndex - x; y++) {
                popFirst = matrix[y][x];
                matrix[y][x] = matrix[x][maxIndex - y];
                matrix[x][maxIndex - y] = matrix[maxIndex - y][maxIndex - x];
                matrix[maxIndex - y][maxIndex - x] = matrix[maxIndex - x][y];
                matrix[maxIndex - x][y] = popFirst;
            }
        }
    }


    private static void verifyMatrixIsSquare(Object[][] matrix) {
        for (Object[] oneRow : matrix) {
            if (matrix.length != oneRow.length)
                throw new RuntimeException("A square matrix is required!");
        }
    }

    private static void printMatrix(Object[][] matrix) {
        System.out.println();
        for (Object[] oneRow : matrix) {
            for (Object oneElement : oneRow) {
                System.out.print(oneElement + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args) {
        String[][] testMatrix = {
                {"9", "8", "7", "6", "5"},
                {"8", "7", "6", "5", "4"},
                {"7", "6", "5", "4", "3"},
                {"6", "5", "4", "3", "2"},
                {"5", "4", "3", "2", "1"}
        };

        String[][] testMatrix2 = {
                {"0", "0", "1", "2", "3", "4"},
                {"0", "0", "0", "1", "2", "3"},
                {"0", "0", "0", "0", "1", "2"},
                {"0", "0", "0", "0", "1", "1"},
                {"0", "0", "0", "0", "0", "0"},
                {"0", "1", "0", "0", "0", "0"}
        };

        printMatrix(testMatrix);
        rotateClockwise(testMatrix);
        printMatrix(testMatrix);
        printMatrix(testMatrix2);
        rotateClockwise(testMatrix2);
        printMatrix(testMatrix2);


    }


}
