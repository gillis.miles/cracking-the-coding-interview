package stringmanip;

import java.util.HashSet;
import java.util.Set;

public class UniqueChars {

    public static boolean isOnlyUniqueCharacters(String s) {
        Set<Character> characterSet = new HashSet<Character>();
        for(char c : s.toCharArray()){
            boolean wasAdded = characterSet.add(c);
            if(!wasAdded)
                return false;
        }
        return true;
    }


    public static void main(String[] args) {

        String isUnique = "abcdefg hijkLMNOP";
        String isNotUnique = "This is a sentence";

        System.out.println("The string '"+isUnique+ "' has only unique characters: " + String.valueOf(isOnlyUniqueCharacters(isUnique)).toUpperCase());
        System.out.println("The string '"+isNotUnique+ "' has only unique characters: " + String.valueOf(isOnlyUniqueCharacters(isNotUnique)).toUpperCase());
    }
}
